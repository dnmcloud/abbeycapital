AWSTemplateFormatVersion: "2010-09-09"

Description: "Assigns default developer permission for development/test environment"

Parameters:
  IAMUsers:
    Type: CommaDelimitedList
    Description: 'Comma separated list of IAM users to add to the default developer dev group'
    Default: ''

Conditions:
  AttachUsersToGroup: !Not [!Equals [!Join ['', !Ref IAMUsers], '']]
  
Resources:
  DefaultDeveloperDevGroup:
    Type: AWS::IAM::Group
    Properties:
      ManagedPolicyArns: 
        - arn:aws:iam::aws:policy/AmazonRDSFullAccess
        - !Ref CloudformationPolicy
        - !Ref StartAndStopTestInstancePolicy
        - !Ref KMSPolicy
        
  AddDevelopersToDevGroup:
    Type: AWS::IAM::UserToGroupAddition
    Condition: AttachUsersToGroup
    Properties:
      GroupName: !Ref DefaultDeveloperDevGroup
      Users: !Ref IAMUsers

  LambdaExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action: sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
        - arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess
        - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        - !Ref KMSPolicy
        
  BuildServerRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action: sts:AssumeRole
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AWSLambdaFullAccess
        - arn:aws:iam::aws:policy/AmazonAPIGatewayAdministrator
        - arn:aws:iam::aws:policy/AWSCloudFormationReadOnlyAccess
        - arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole
        - !Ref CloudformationPolicy
        - !Ref KMSPolicy

  BuildServerS3Policy:
    Type: AWS::IAM::Policy
    Properties:
      Roles:
        - !Ref BuildServerRole
      PolicyName: BuildArtifactsToS3
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          Effect: Allow
          Action:
            - s3:PutObject
            - s3:GetBucketLocation
          Resource:
            - arn:aws:s3:::abbeycapital/*
            - arn:aws:s3:::abbeycapital

  BuildServerResourceCreationPolicy:
    Type: AWS::IAM::Policy
    Properties:
      Roles: 
        - !Ref BuildServerRole
      PolicyName: ResourceCreation
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action: 
              - s3:*
            Resource:
              - "*"
          - Effect: Allow
            Action:
              - sqs:*
            Resource:
              - "*"
          # - Effect: Allow
          #   Action: ec2:CreateNetworkInterface
          #   Resource: "*" 

  BuildServerInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Roles:
        - !Ref BuildServerRole

  CloudformationPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      Description: Allows developers to perform Cloudformation operations
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action:
              - cloudformation:CreateUploadBucket
              - cloudformation:EstimateTemplateCost
              - cloudformation:ListExports
              - cloudformation:ListStacks
              - cloudformation:ListImports
              - cloudformation:DescribeAccountLimits
              - cloudformation:ValidateTemplate
              - cloudformation:GetTemplateSummary
            Resource: "*"
          - Effect: Allow
            Action:
              - cloudformation:CreateChangeSet
              - cloudformation:DeleteChangeSet
              # Is there an argument for restricting this to test stacks only? If dev can't create resources in change set, it'll just fail and rollback
              - cloudformation:ExecuteChangeSet
            Resource:
                - !Sub arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/*/*
                - !Sub arn:aws:cloudformation:${AWS::Region}:aws:transform/Serverless-2016-10-31
          - Effect: Allow
            Action:
              - cloudformation:DeleteStack
              - cloudformation:CreateStack
            Resource:
                # Initiating these actions for things a user can't do, like terminate an instance, for example will fail and rollback
                - !Sub arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/*test*/*
                - !Sub arn:aws:cloudformation:${AWS::Region}:${AWS::AccountId}:stack/*Test*/*

  KMSPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      Description: KMS access
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Action:
              - kms:Decrypt
              - kms:Encrypt
              - kms:GenerateDataKey
            Resource:
              - !ImportValue KMSKeyStack-S3KeyARN
              - !ImportValue KMSKeyStack-SQSKeyARN

  StartAndStopTestInstancePolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      Description: Allow developers to start, stop and restart test instances
      PolicyDocument:
        Version: "2012-10-17"
        Statement:
        - Effect: Allow
          Action:
            - ec2:StopInstances
            - ec2:StartInstances
            - ec2:RebootInstances
          Resource:
            - !Sub arn:aws:ec2:${AWS::Region}:${AWS::AccountId}:instance/*
          Condition:
            StringEquals:
              ec2:ResourceTag/Environment: Test

Outputs:
  DevelopmentLambdaRole:
    Value: !GetAtt LambdaExecutionRole.Arn
    Export:
      Name : !Sub ${AWS::StackName}-LambdaExecutionRole

# Run using (line breaks added for clarity...)
# aws cloudformation create-change-set 
#   --stack-name Development-UserPermissions 
#   --template-body file://CFN-Templates\DefaultDeveloperDevGroup.yaml 
#   --parameters file://Params\DefaultDeveloperGroup.json
#   --change-set-name your-change-set-name-here
#   --capabilities CAPABILITY_IAM

