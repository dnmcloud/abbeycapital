[CmdletBinding()]
param(

    [Parameter(Mandatory=$true)]
    [string]
    $DCBIOSName,

    [Parameter(Mandatory=$true)]
    [string]
    $DCUsername,

    [Parameter(Mandatory=$true)]
    [string]
    $DCPassword,

    [Parameter(Mandatory=$false)]
    [string]
    $MyWindowsNameAZ1,

    [Parameter(Mandatory=$false)]
    [string]
    $MyWindowsNameAZ2,

    [Parameter(Mandatory=$false)]
    [string]
    $WSFCNode3NetBIOSName=$null,

    [Parameter(Mandatory=$false)]
    [string]
    $WSFCNode1PrivateIP2,

    [Parameter(Mandatory=$false)]
    [string]
    $WSFCNode2PrivateIP2,

    [Parameter(Mandatory=$false)]
    [string]
    $WSFCNode3PrivateIP2=$null,

    [Parameter(Mandatory=$false)]
    [string]
    $NetBIOSName

)
try {
    Start-Transcript -Path C:\cfn\log\Configure-WSFC.ps1.txt -Append
    $ErrorActionPreference = "Stop"

    $DomainAdminFullUser = $DCBIOSName + '\' + $DCUsername
    $DomainAdminSecurePassword = ConvertTo-SecureString $DCPassword -AsPlainText -Force
    $DomainAdminCreds = New-Object System.Management.Automation.PSCredential($DomainAdminFullUser, $DomainAdminSecurePassword)

    $ConfigWSFCPs={
        $nodes = $Using:MyWindowsNameAZ1, $Using:MyWindowsNameAZ2
        $addr =  $Using:WSFCNode1PrivateIP2, $Using:WSFCNode2PrivateIP2
        New-Cluster -Name WSFCluster1 -Node $nodes -StaticAddress $addr
    }
    if ($WSFCNode3NetBIOSName) {
        $ConfigWSFCPs={
            $nodes = $Using:MyWindowsNameAZ1, $Using:MyWindowsNameAZ2, $Using:WSFCNode3NetBIOSName
            $addr =  $Using:WSFCNode1PrivateIP2, $Using:WSFCNode2PrivateIP2, $Using:WSFCNode3PrivateIP2
            New-Cluster -Name WSFCluster1 -Node $nodes -StaticAddress $addr
        }
    }

    Invoke-Command -Authentication Credssp -Scriptblock $ConfigWSFCPs -ComputerName $NetBIOSName -Credential $DomainAdminCreds
}
catch {
    Write-Verbose "$($_.exception.message)@ $(Get-Date)"
}
