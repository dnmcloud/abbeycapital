[CmdletBinding()]
param(

    [Parameter(Mandatory=$true)]
    [string]$DCBIOSName,

    [Parameter(Mandatory=$true)]
    [string]$DCUsername,

    [Parameter(Mandatory=$true)]
    [string]$DCPassword,

    [Parameter(Mandatory=$true)]
    [string]$MyWindowsNameAZ2,

    [Parameter(Mandatory=$false)]
    [string]$FileServerNetBIOSName,

    [Parameter(Mandatory=$false)]
    [bool]$Witness=$true

)
try {
    Start-Transcript -Path C:\cfn\log\Set-ClusterQuorum.ps1.txt -Append
    $ErrorActionPreference = "Stop"

    $DomainAdminFullUser = $DCBIOSName + '\' + $DCUsername
    $DomainAdminSecurePassword = ConvertTo-SecureString $DCPassword -AsPlainText -Force
    $DomainAdminCreds = New-Object System.Management.Automation.PSCredential($DomainAdminFullUser, $DomainAdminSecurePassword)

    $SetClusterQuorum={
        $ErrorActionPreference = "Stop"
        if ($Using:Witness) {
            $ShareName = "\\" + $Using:FileServerNetBIOSName + "\witness"
            Set-ClusterQuorum -NodeAndFileShareMajority $ShareName
        } else {
            Set-ClusterQuorum -NodeMajority
        }
    }

    Invoke-Command -Scriptblock $SetClusterQuorum -ComputerName $MyWindowsNameAZ2 -Credential $DomainAdminCreds

}
catch {
    Write-Verbose "$($_.exception.message)@ $(Get-Date)"
}
