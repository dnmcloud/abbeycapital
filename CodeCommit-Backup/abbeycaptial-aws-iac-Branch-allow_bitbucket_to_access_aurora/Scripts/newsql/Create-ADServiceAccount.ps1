[CmdletBinding()]
param(

    [Parameter(Mandatory=$true)]
    [string]$DCBIOSName,

    [Parameter(Mandatory=$true)]
    [string]$DCUsername,

    [Parameter(Mandatory=$true)]
    [string]$DCPassword,

    [Parameter(Mandatory=$true)]
    [string]$DCDNSName,

    [Parameter(Mandatory=$true)]
    [string]$ServiceAccountUser,

    [Parameter(Mandatory=$true)]
    [string]$ServiceAccountPassword,

    [Parameter(Mandatory=$true)]
    [string]$ADServerNetBIOSName

)

try {
    Start-Transcript -Path C:\cfn\log\Create-ADServiceAccount.ps1.txt -Append
    $ErrorActionPreference = "Stop"

    $DomainAdminFullUser = $DCBIOSName + '\' + $DCUsername
    $ServiceAccountFullUser = $DCBIOSName + '\' + $ServiceAccountUser
    $DomainAdminSecurePassword=$DCPassword|ConvertTo-SecureString  -AsPlainText -Force
    $DomainAdminCreds = New-Object System.Management.Automation.PSCredential($DomainAdminFullUser, $DomainAdminSecurePassword)
    $ServiceAccountSecurePassword=$ServiceAccountPassword|ConvertTo-SecureString  -AsPlainText -Force
    $UserPrincipalName = $ServiceAccountUser + "@" + $DCDNSName
    $createUserSB = {
        $ErrorActionPreference = "Stop"
        if (-not (Get-Module -ListAvailable -Name ActiveDirectory)) {
            Install-WindowsFeature RSAT-AD-PowerShell
        }
        Write-Host "Searching for user $Using:ServiceAccountUser"
        if (Get-ADUser -Filter {sAMAccountName -eq $Using:ServiceAccountUser}) {
            Write-Host "User already exists."
            # Ensure that password is correct for the user
            if ((New-Object System.DirectoryServices.DirectoryEntry "", $Using:ServiceAccountFullUser, $Using:ServiceAccountPassword).PSBase.Name -eq $null) {
                throw "The password for $Using:ServiceAccountUser is incorrect"
            }
        } else {
            Write-Host "Creating user $Using:ServiceAccountUser"
            New-ADUser -Name $Using:ServiceAccountUser -UserPrincipalName $Using:UserPrincipalName -AccountPassword $Using:ServiceAccountSecurePassword -Enabled $true -PasswordNeverExpires $true
        }
    }

    try {
        Write-Host "Invoking command on $ADServerNetBIOSName"
        Invoke-Command -ScriptBlock $createUserSB -ComputerName $ADServerNetBIOSName -Credential $DomainAdminCreds
    }
    catch {
        Write-Host $_
        Write-Host "Retrying user creation with CredSSP."
        Invoke-Command -ScriptBlock $createUserSB -ComputerName $ADServerNetBIOSName -Credential $DomainAdminCreds -Authentication Credssp
    }
}
catch {
    Write-Verbose "$($_.exception.message)@ $(Get-Date)"
}
