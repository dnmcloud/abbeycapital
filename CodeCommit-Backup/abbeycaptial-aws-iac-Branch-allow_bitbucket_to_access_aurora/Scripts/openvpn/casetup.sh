# Change the BUCKET_NAME to whatever the s3 bucket name
BUCKET_NAME=abbeycapital-restricted-deployment-bucket

# Change region
REGION=eu-west-1

# Change the VPN name
mynm=abbeycapital-vpn

[ -z "$USERLIST" ] && USERLIST=""

cd /etc/openvpn/easy-rsa/
. ./vars
./clean-all

# code below borrowed from https://github.com/Nyr/openvpn-install/blob/master/openvpn-install.sh
#./build-ca
# The following lines are from build-ca. I don't use that script directly
# because it's interactive and we don't want that.
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" --initca $mynm
#./build-key-server $mynm
# theres a bug that seems to cause a problem when not running interactively fix it
# either do this
# perl -p -i -e 's|^(subjectAltName=)|#$1|;' /etc/openvpn/easy-rsa/openssl-1.0.0.cnf
# or set export KEY_ALTNAMES=$KEY_EMAIL in the vars - assume we have done this
# Same as the last time, we are going to run build-key-server
export EASY_RSA="${EASY_RSA:-.}"
"$EASY_RSA/pkitool" --server $mynm
# openssl dhparam needs a home dir to write .rnd for it to work
# setting this avoids the error "unable to write 'random state'"
export RANDFILE=/root/.rnd
./build-dh

# all done now - lets get the keys
cd keys/
cp $mynm.crt $mynm.key ca.crt dh*.pem /etc/openvpn/
# now lets get the client keys
cd /etc/openvpn/easy-rsa/
. ./vars
# ./build-key dummy
"$EASY_RSA/pkitool" dummy
# ./build-key dnmsupport
"$EASY_RSA/pkitool" dnmsupport
# ./build-key ncreighton
"$EASY_RSA/pkitool" ncreighton
# ./build-key ksims
"$EASY_RSA/pkitool" ksims
# ./build-key mkopicz
"$EASY_RSA/pkitool" mkopicz
# ./build-key jross
"$EASY_RSA/pkitool" jross
# ./build-key jtodea
"$EASY_RSA/pkitool" jtodea
# ./build-key rfogarty
"$EASY_RSA/pkitool" rfogarty

# Add users defined in env variable $USERLIST
for USERNAME in `echo $USERLIST`
do
    "$EASY_RSA/pkitool" $USERNAME
done

# now revoke dummy cert and copy the crl.pem
"$EASY_RSA/revoke-full" dummy
cp keys/crl.pem /etc/openvpn

echo "Backing up server keys"
cd /etc/openvpn
tar -cvf /root/ca.tar ca.crt $mynm.crt $mynm.key dh*.pem crl.pem

echo "Backing vpn user keys"
cd /etc/openvpn/easy-rsa
tar -cvf /root/keys.tar keys
# Copy ca.tar and keys.tar files upto s3 bucket name
aws s3 cp /root/ca.tar s3://${BUCKET_NAME}/openvpn/ --region ${REGION}
aws s3 cp /root/keys.tar s3://${BUCKET_NAME}/openvpn/ --region ${REGION}
