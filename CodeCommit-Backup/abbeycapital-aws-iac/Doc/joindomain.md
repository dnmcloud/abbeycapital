```powershell
 $instanceId = (Invoke-WebRequest http://169.254.169.254/latest/meta-data/instance-id -UseBasicParsing).Content
 aws ssm send-command --document-name "AWS-JoinDirectoryServiceDomain" --instance-ids $instanceId --parameters '{\"dnsIpAddresses\":[\"10.140.13.100\",\"10.140.12.182\"],\"directoryId\":[\"d-9367295973\"],\"directoryName\":[\"abbeycapital.local\"]}' --timeout-seconds 600 --region eu-west-1
```

IAM or role needs following policy

````json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ssm:SendCommand",
            "Resource": [
                "arn:aws:ec2:*:*:instance/*",
                "arn:aws:ssm:*:*:document/AWS-JoinDirectoryServiceDomain"
            ]
        }
    ]
}
```
