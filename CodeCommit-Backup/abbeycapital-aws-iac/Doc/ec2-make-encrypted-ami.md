* [AWS SSM Plugin](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-install-plugin.html)
* [Using AWS SSM](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager-working-with-sessions-start.html)
* [AWS EC2 CLI run-instances](https://docs.aws.amazon.com/cli/latest/reference/ec2/run-instances.html)

Launch an instance. It needs an instance profile role that supports SSM.

```bash
aws ec2 run-instances
    --image-id ami-0e60ba06451c1b981
    --instance-type t2.micro
    --iam-instance-profile Arn=arn:aws:iam::985715988019:instance-profile/SSM-EC2
    --subnet-id subnet-02b05ba5633c31c78
```

Check the current state of the instance. It needs to be running.

```bash
aws ec2 describe-instance-status
    --instance-ids i-0d88e350ad0582d04
```

Use SSM to connect to the instance 

```bash
aws ssm start-session --target i-0d88e350ad0582d04
```

Tell the instance to reinitialise itself on launch. Important for retrieving the local Admin password

```powershell
C:\ProgramData\Amazon\EC2-Windows\Launch\Scripts\InitializeInstance.ps1 -Schedule
exit
```

Stop the running instance

```bash
aws ec2 stop-instances
    --instance-ids i-0d88e350ad0582d04
```

Create an AMI from the instance

```bash
aws ec2 create-image
    --name Win2019
    --instance-id i-0d88e350ad0582d04
```

Create an encrypted copy of the image

```bash
aws ec2 copy-image
    --source-image-id ami-08b765b0bff3f8493
     --name win-2019-encrypted 
    --encrypted
    --kms-key-id 0c93b06e-a36a-45a3-8162-bde552667dbf 
    --source-region eu-west-1
```

Deregister the original unencrypted image.

```bash
aws ec2 deregister-image --image-id ami-08b765b0bff3f8493
```