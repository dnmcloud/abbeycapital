servername=$1
serverid=$2
loggroupname=abbeycapital-instance-logs
filtername=$serverid-$servername-encountered-reboot
metricName=$servername-event-log-service
metricNamespace=LogMetrics
metricValue=1
defaultMetricValue=0

aws logs put-metric-filter --log-group-name $loggroupname --filter-name $filtername --filter-pattern "[log, type, eventid=6006, useraccount, server=$servername*, message]" --metric-transformations metricName=$metricName,metricNamespace=$metricNamespace,metricValue=$metricValue,defaultValue=$defaultMetricValue


aws cloudwatch put-metric-alarm --region eu-west-1 --alarm-name $filtername --alarm-description $filtername --metric-name $metricName --namespace $metricNamespace --statistic Sum --period 300 --threshold $defaultMetricValue --comparison-operator GreaterThanThreshold --evaluation-periods 1 --alarm-actions "arn:aws:sns:eu-west-1:985715988019:P2notification" --ok-actions "arn:aws:sns:eu-west-1:985715988019:P2notification"