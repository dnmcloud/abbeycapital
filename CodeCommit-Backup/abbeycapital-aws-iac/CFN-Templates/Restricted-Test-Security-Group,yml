AWSTemplateFormatVersion: 2010-09-09
Description: System security groups

Parameters:
  Environment:
    Description: 'Test or production?'
    Type: String
    Default: 'Prod'
    AllowedValues:
      - 'Test'
      - 'Prod'

Resources:
  RestrictedTestSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: "security group for test restored systems"
      VpcId: !ImportValue "VPC-10140-VPCBase-VPCId"
      SecurityGroupIngress:
        # You need to define ingress rules for each port range for each CIDR/SG
        - IpProtocol: tcp
          FromPort: 3389
          ToPort: 3389
          CidrIp: '10.140.5.8/32'
          Description: RDP for workspaces		
        - IpProtocol: tcp
          FromPort: 3389
          ToPort: 3389
          CidrIp: '10.140.4.173/32'
          Description: RDP for workspaces	
      SecurityGroupEgress:
        - IpProtocol: tcp
          FromPort: '53'
          ToPort: '53'
          Description: 'DNS port'
          CidrIp: '8.8.8.8/32'
        - IpProtocol: tcp
          FromPort: '443'
          ToPort: '443'
          Description: 'HTTPS'
          CidrIp: '0.0.0.0/0'
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          Description: 'HTTP'
          CidrIp: '0.0.0.0/0'

      Tags:
        - Key: Name
          Value: !Sub Restricted-${Environment}
        - Key: Environment
          Value: !Ref 'Environment'
        - Key: Purpose
          Value: 'Restored systems'

Outputs:
  OutRestrictedTestSecurityGroup:
    Value: !Ref RestrictedTestSecurityGroup
    Description: Restricted Test Systems Sg
    Export:
      Name: !Sub ${AWS::StackName}-GroupId
