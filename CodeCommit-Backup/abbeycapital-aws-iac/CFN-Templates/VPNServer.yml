AWSTemplateFormatVersion: '2010-09-09'
Description: >-
  For AZ1 -> EIP, ENI, EC2 ! For AZ2 -> PublicIP ! for all create instance with OpenVPN
  software. The security group, s3 bucket and instanceprofile with access to the s3
  bucket must already exist in the VPC. Scripts, certs and keys should have been created
  by a CAServer and saved into the s3 bucket.
Parameters:
  envPrefix:
    Description: Environment name prefix.
    Type: String
  MyInstanceType:
    Description: VPNServer EC2 instance type
    Type: String
    AllowedValues:
      - t2.micro
      - t3.micro
      - m1.small
      - m1.medium
      - m1.large
      - m1.xlarge
      - m2.xlarge
      - m2.2xlarge
      - m2.4xlarge
      - m3.xlarge
      - m3.2xlarge
      - c1.medium
      - c1.xlarge
      - cc1.4xlarge
      - cc2.8xlarge
      - cg1.4xlarge
    ConstraintDescription: must be a valid EC2 instance type.
  MyKeyName:
    Description: The EC2 Key Pair to allow SSH access to the instances
    Type: String
  MyPrivateIpAddress:
    Description: Private IP Address to assign - leave blank for auto assignment
    Type: String
    MinLength: '0'
    MaxLength: '15'
  MyPublicIpAddress:
    Description: Public IP Address to assign - leave blank for auto assignment
    Type: String
    MinLength: '0'
    MaxLength: '15'
    AllowedValues:
      - 'yes'
      - 'no'
  MyVPCCidrRange:
    Description: The CIDR range of my VPC
    Type: String
    MinLength: '9'
    MaxLength: '18'
    AllowedPattern: (\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})
    ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.
  MySubnetId:
    Type: String
    Description: SubnetId of an existing subnet in your Virtual Private Cloud (VPC)
  MyVPNSgId:
    Description: Id of an existing EC2 VPN security group
    Type: String
  MyInstanceProfile:
    Description: Instance Profile that will allow me to getfiles from s3
    Type: String
  RestrictedfilesBucket:
    Description: bucket that has the files I need
    Type: String
  MyConfigfilesBucket:
    Description: bucket that has the files I need
    Type: String
  MyInstanceRole:
    Description: Role that the profile gives me access to
    Type: String
  MyS3AnsibleTarFile:
    Description: Ansible tar filename in s3 (including s3 bucket name)
    Type: String
  MyAnsiblePackageName:
    Description: Ansible package name
    Type: String
  MyCloudWatchLogGroup:
    Description: CloudWatch log group send logs to
    Type: String
Mappings:
  RegionMap:
    eu-west-1:
      AMI: ami-bff32ccc
Conditions:
  RequestedPrivateIP: !Not
    - !Equals
      - !Ref 'MyPrivateIpAddress'
      - ''
  RequestedPublicIP: !Equals
    - !Ref 'MyPublicIpAddress'
    - 'no'
Resources:
  IPAddress:
    Type: AWS::EC2::EIP
    Condition: RequestedPublicIP
    Properties:
      Domain: vpc
  AssociateEIP:
    Type: AWS::EC2::EIPAssociation
    Condition: RequestedPublicIP
    Properties:
      AllocationId: !GetAtt 'IPAddress.AllocationId'
      NetworkInterfaceId: !Ref 'publicInterface'
  publicInterface:
    Type: AWS::EC2::NetworkInterface
    Condition: RequestedPublicIP
    Properties:
      SubnetId: !Ref 'MySubnetId'
      PrivateIpAddress: !If
        - RequestedPrivateIP
        - !Ref 'MyPrivateIpAddress'
        - !Ref 'AWS::NoValue'
      Description: !Join
        - '-'
        - - !Ref 'envPrefix'
          - VPNBastionServer-network-interface
      GroupSet:
        - !Ref 'MyVPNSgId'
      SourceDestCheck: 'true'
      Tags:
        - Key: Name
          Value: !Join
            - '-'
            - - !Ref 'envPrefix'
              - VPNBastionServer-network-interface
  VPNInstance:
    Type: AWS::EC2::Instance
    Metadata:
      AWS::CloudFormation::Authentication:
        S3AccessCreds:
          type: S3
          roleName: !Ref 'MyInstanceRole'
          buckets:
            - !Ref 'MyConfigfilesBucket'
            - !Ref 'RestrictedfilesBucket'
      AWS::CloudFormation::Init:
        configSets:
          defaultConfigSet:
            - install_packages
            - configure_hostname
            - configure_localtime
            - configure_cloudformation
            - install_cloudwatch_logging
            - install_cloudwatch_metrics
            - configure_ansible_etc_hosts
            - config_openvpn
          additionalConfigSet:
            - additional_configuration
        install_packages:
          packages:
            yum:
              perl-DateTime: []
              perl-Sys-Syslog: []
              perl-LWP-Protocol-https: []
              perl-Switch: []
              perl-URI: []
              perl-Bundle-LWP: []
              sssd: []
              realmd: []
              krb5-workstation: []
              git: []
        configure_hostname:
          files:
            /tmp/myrclocal:
              content: !Join
                - ''
                - - "# Remove line containing #my-host-name-do-not-remove-me from\
                    \ the /etc/hosts file\n"
                  - "/bin/sed -i -e '/#my-host-name-do-not-remove-me/d' /etc/hosts\n"
                  - "# Get IP address and hostname\n"
                  - "IPADDRESS=$(/opt/aws/bin/ec2-metadata | /usr/bin/awk '/local-ipv4/{\
                    \ print $2 }')\n"
                  - "HOSTNAME=$(/bin/hostname)\n"
                  - "# Add IP address and hostname to the /etc/hosts file\n"
                  - "echo \"$IPADDRESS $HOSTNAME #my-host-name-do-not-remove-me\"\
                    \ >>/etc/hosts\n"
              mode: '000755'
              owner: root
              group: root
          commands:
            configure_hostname-cmd1:
              command: sed -i '/exit 0/d' /etc/rc.d/rc.local
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            configure_hostname-cmd2:
              command: cat /tmp/myrclocal >>/etc/rc.local
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            configure_hostname-cmd3:
              command: /etc/rc.local
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
        configure_localtime:
          files:
            /tmp/myclock:
              content: !Join
                - ''
                - - "ZONE=\"GB-Eire\"\n"
                  - "UTC=true\n"
              mode: '000644'
              owner: root
              group: root
          commands:
            configure_localtime-cmd1:
              command: cp /tmp/myclock /etc/sysconfig/clock
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            configure_localtime-cmd2:
              command: ln -sf /usr/share/zoneinfo/GB-Eire /etc/localtime
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
        configure_cloudformation:
          files:
            /etc/cfn/cfn-hup.conf:
              content: !Join
                - ''
                - - "[main]\n"
                  - stack=
                  - !Ref 'AWS::StackName'
                  - "\n"
                  - region=
                  - !Ref 'AWS::Region'
                  - "\n"
                  - "interval=15\n"
              mode: '000400'
              owner: root
              group: root
            /etc/cfn/hooks.d/cfn-auto-reloader.conf:
              content: !Join
                - ''
                - - "[cfn-auto-reloader-hook]\n"
                  - "triggers=post.update\n"
                  - "path=Resources.Ec2Instance.Metadata.AWS::CloudFormation::Init\n"
                  - 'action=cfn-init -c additionalConfigSet -s '
                  - !Ref 'AWS::StackName'
                  - ' -r Ec2Instance '
                  - ' --region '
                  - !Ref 'AWS::Region'
                  - "\n"
        install_cloudwatch_logging:
          files:
            /root/awslogs.conf:
              content: !Join
                - ''
                - - "[general]\n"
                  - "state_file = /var/awslogs/state/agent-state\n"
                  - "\n"
                  - "[/var/log/messages]\n"
                  - "file = /var/log/messages\n"
                  - 'log_group_name = '
                  - !Ref 'MyCloudWatchLogGroup'
                  - "\n"
                  - "log_stream_name = {instance_id}/messages\n"
                  - "datetime_format = %b %d %H:%M:%S\n"
                  - "buffer_duration = 600000\n"
              mode: '000400'
              owner: root
              group: root
          commands:
            install_cloudwatch_logging-cmd1:
              command: wget https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            install_cloudwatch_logging-cmd2:
              command: chmod +x ./awslogs-agent-setup.py
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
        install_cloudwatch_metrics:
          sources:
            /root: http://ec2-downloads.s3.amazonaws.com/cloudwatch-samples/CloudWatchMonitoringScripts-v1.1.0.zip
          files:
            /root/aws-scripts-mon/cloudwatch_crontab_entry.cron:
              content: !Join
                - ''
                - - >-
                    */5 * * * * /root/aws-scripts-mon/mon-put-instance-data.pl --mem-util
                    --mem-used --mem-avail --swap-util --swap-used --disk-path=/ --disk-path=/var/opt
                    --disk-space-util --disk-space-used --disk-space-avail --from-cron
                    --aws-iam-role=
                  - !Ref 'MyInstanceRole'
                  - "\n"
              mode: '000400'
              owner: root
              group: root
          commands:
            install_cloudwatch_metrics-cmd1:
              command: rm -f /var/tmp/aws-mon/instance_id
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            install_cloudwatch_metrics-cmd2:
              command: chmod +x /root/aws-scripts-mon/*.pl
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            install_cloudwatch_metrics-cmd3:
              command: crontab /root/aws-scripts-mon/cloudwatch_crontab_entry.cron
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
        configure_ansible_etc_hosts:
          files:
            /etc/ansible/hosts:
              content: !Join
                - ''
                - - "[local]\n"
                  - "localhost ansible_connection=local\n"
              mode: '000400'
              owner: root
              group: root
        config_openvpn:
          packages:
            yum:
              openvpn: []
          files:
            /tmp/vpnsetup.sh:
              source: !Join
                - ''
                - - http://
                  - !Ref 'RestrictedfilesBucket'
                  - .s3.amazonaws.com/openvpn/
                  - !Ref 'envPrefix'
                  - /vpnsetup.sh
              mode: '000755'
              owner: root
              group: root
            /root/server.conf:
              source: !Join
                - ''
                - - http://
                  - !Ref 'RestrictedfilesBucket'
                  - .s3.amazonaws.com/openvpn/server.conf
              mode: '000644'
              owner: root
              group: root
            /root/ca.tar:
              source: !Join
                - ''
                - - http://
                  - !Ref 'RestrictedfilesBucket'
                  - .s3.amazonaws.com/openvpn/ca.tar
              mode: '000644'
              owner: root
              group: root
          commands:
            cmdsvpn1:
              command: cp /root/server.conf /etc/openvpn/server.conf
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            cmdsvpn2:
              command: tar -xvf /root/ca.tar
              env:
                MyEnvVars: No special
              cwd: /etc/openvpn
              ignoreErrors: 'false'
            cmdsvpn3:
              command: cat /tmp/vpnsetup.sh | tr -d '\015' >/root/vpnsetup.sh; chmod
                755 /root/vpnsetup.sh
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            cmdsvpn4:
              command: /root/vpnsetup.sh
              env:
                MyEnvVars: No special
              cwd: /etc/openvpn
              ignoreErrors: 'false'
            cmdsvpn5:
              command: >-
                echo /sbin/modprobe iptable_nat >>/etc/rc.local; echo 'echo 1 | tee
                /proc/sys/net/ipv4/ip_forward' >>/etc/rc.local; echo /sbin/iptables
                -t nat -A POSTROUTING -s 10.8.0.0/24 -o eth0 -j MASQUERADE >>/etc/rc.local;
                echo /sbin/iptables -t nat -A POSTROUTING -s ${VPCCidrRange} -o eth0
                -j MASQUERADE >>/etc/rc.local
              env:
                VPCCidrRange: !Ref 'MyVPCCidrRange'
              cwd: /root
              ignoreErrors: 'true'
            cmdsvpn6:
              command: /etc/rc.local
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
          services:
            sysvinit:
              openvpn:
                enabled: 'true'
                ensureRunning: 'true'
                files:
                  - /etc/openvpn/mycrl.pem
                  - /etc/openvpn/server.conf
        additional_configuration:
          commands:
            additional_configuration-cmd1:
              command: echo change this number [1] to get this to be run in cfn_hup
                >/root/dummy.txt
              env:
                MyEnvVars: No special
              cwd: /root
              ignoreErrors: 'false'
            additional_configuration-cmd2:
              command: aws s3 cp s3://${ANSIBLE_TARFILE} . --region eu-west-1
              env:
                ANSIBLE_TARFILE: !Ref 'MyS3AnsibleTarFile'
              cwd: /root
              ignoreErrors: 'false'
            additional_configuration-cmd3:
              command: tar -xf ${ANSIBLE_PACKAGE_NAME}.tar
              env:
                ANSIBLE_PACKAGE_NAME: !Ref 'MyAnsiblePackageName'
              cwd: /root
              ignoreErrors: 'false'
            additional_configuration-cmd4:
              command: su -c "/usr/local/bin/ansible-playbook /root/${ANSIBLE_PACKAGE_NAME}/playbook.yml"
              env:
                ANSIBLE_PACKAGE_NAME: !Ref 'MyAnsiblePackageName'
              cwd: /root
              ignoreErrors: 'false'
    Properties:
      ImageId: !FindInMap
        - RegionMap
        - !Ref 'AWS::Region'
        - AMI
      InstanceType: !Ref 'MyInstanceType'
      IamInstanceProfile: !Ref 'MyInstanceProfile'
      KeyName: !Ref 'MyKeyName'
      Tags:
        - Key: Name
          Value: !Join
            - '-'
            - - !Ref 'envPrefix'
              - VPNBastionServer-AZ1
              - !If
                - RequestedPublicIP
                - AZ1
                - AZ2
        - Key: 'Purpose'
          Value: 'DNM Support Access'
        - Key: 'Environment'
          Value: 'Prod'
      NetworkInterfaces:
        - AssociatePublicIpAddress: !If
            - RequestedPublicIP
            - !Ref 'AWS::NoValue'
            - 'true'
          NetworkInterfaceId: !If
            - RequestedPublicIP
            - !Ref 'publicInterface'
            - !Ref 'AWS::NoValue'
          GroupSet:
            - !If
              - RequestedPublicIP
              - !Ref 'AWS::NoValue'
              - !Ref 'MyVPNSgId'
          SubnetId: !If
            - RequestedPublicIP
            - !Ref 'AWS::NoValue'
            - !Ref 'MySubnetId'
          DeviceIndex: '0'
      UserData: !Base64
        Fn::Join:
          - ''
          - - "#!/bin/bash\n"
            - "## Error reporting helper function\n"
            - "function error_exit\n"
            - "{\n"
            - '   /opt/aws/bin/cfn-signal -e 1 -r "$1" '''
            - !Ref 'VPNInstanceWaitHandle'
            - "'\n"
            - "   exit 1\n"
            - "}\n"
            - "yum update -y aws-cfn-bootstrap\n"
            - "yum update -y \n"
            - "## Pip install ansible\n"
            - "pip install ansible\n"
            - "## Initialize CloudFormation bits\n"
            - '/opt/aws/bin/cfn-init -c defaultConfigSet -v -s '
            - !Ref 'AWS::StackName'
            - ' -r VPNInstance'
            - ' --region '
            - !Ref 'AWS::Region'
            - " || error_exit 'Failed to run cfn-init'\n"
            - "## Do this setup here because it does not work from metadata\n"
            - '/root/awslogs-agent-setup.py -n -r '
            - !Ref 'AWS::Region'
            - " -c /root/awslogs.conf\n"
            - "## Start up the cfn-hup daemon to listen for changes to the Server\
              \ metadata\n"
            - "/opt/aws/bin/cfn-hup || error_exit 'Failed to start cfn-hup'\n"
            - "## CloudFormation signal that setup is complete\n"
            - /opt/aws/bin/cfn-signal -e 0 -r "Instance setup complete" '
            - !Ref 'VPNInstanceWaitHandle'
            - "'\n"
            - "echo userdata-complete; \n"
  VPNInstanceWaitHandle:
    Type: AWS::CloudFormation::WaitConditionHandle
  VPNInstanceWaitCondition:
    Type: AWS::CloudFormation::WaitCondition
    DependsOn: VPNInstance
    Properties:
      Handle: !Ref 'VPNInstanceWaitHandle'
      Timeout: '1800'
Outputs:
  OutInstanceId:
    Description: InstanceId of the newly created EC2 instance
    Value: !Ref 'VPNInstance'
    Export:
      Name: !Sub '${AWS::StackName}-VPNInstance'
  OutAZ:
    Description: Availability Zone of the newly created EC2 instance
    Value: !GetAtt 'VPNInstance.AvailabilityZone'
    Export:
      Name: !Sub '${AWS::StackName}-AvailabilityZone'
  OutPrivateIP:
    Description: Private IP address of the newly created EC2 instance
    Value: !GetAtt 'VPNInstance.PrivateIp'
    Export:
      Name: !Sub '${AWS::StackName}-PrivateIp'
  OutElasticIP:
    Condition: RequestedPublicIP
    Description: Elastic IP address of the newly created EC2 instance
    Value: !Ref 'IPAddress'
    Export:
      Name: !Sub '${AWS::StackName}-PublicEIPAddress'
