Description: Process MRM Sync Events from an SQS Queue.
Parameters:
  Subnets:
    Type: List<AWS::EC2::Subnet::Id>
    Description: Lambda subnets
    Default: subnet-02b05ba5633c31c78, subnet-059895e3c9b357096
  LambdaRole:
    Type: String
    Description: Role the lambda should use (when not in production)
    Default: arn:aws:iam::985715988019:role/lambda-execution-role-serverlessdevGrp
  Environment:
    Default: Test
    Type: String
    Description: Test or production? You decide.
    AllowedValues:
      - Test
      - Prod
  WarehouseDBHostName:
    Type: String
    Description: Hostname of server containing MRM database
    Default: abbey15dev.abbeycapital.local
  DBPasswordSettingName:
    Type: String
    Description: Name for DB password of MRM database stored in SSM
    Default: TestMRMDatabasePassword
  QueueName:
    Type: String
    Description: Name to give the queue used to publish MRM changes to.
    Default: 'MRM-Sync'
  DBHostname:
    Type: String
    Description: Hostname of server containing MRM database
    Default: 'abbey2dev.abbeycapital.local'
AWSTemplateFormatVersion: '2010-09-09'
Outputs:
  MRMSyncQueueUrl:
    Export:
      Name: !Join
        - '-'
        - - !Ref 'AWS::StackName'
          - !Ref 'QueueName'
    Value: !Ref 'SyncQueue'
Conditions:
  IsProduction: !Equals
    - !Ref 'Environment'
    - Prod
Resources:
  SyncQueue:
    Type: AWS::SQS::Queue
    Properties:
      RedrivePolicy:
        deadLetterTargetArn: !GetAtt 'DeadLetterQueue.Arn'
        maxReceiveCount: 1
      KmsMasterKeyId: !ImportValue 'KMSKeyStack-SQSKey'
      QueueName: !Sub '${Environment}-${QueueName}'
      Tags:
        - Value: !Ref 'Environment'
          Key: Environment
  DeadLetterAlarm:
    Type: AWS::CloudWatch::Alarm
    Properties:
      EvaluationPeriods: 1
      TreatMissingData: missing
      Dimensions:
        - Name: QueueName
          Value: !GetAtt 'DeadLetterQueue.QueueName'
      AlarmDescription: !Sub 'If there are more than 10 messages in queue ${DeadLetterQueue.QueueName},
        something is wrong.'
      Namespace: AWS/SQS
      Period: 300
      ComparisonOperator: GreaterThanThreshold
      Statistic: Sum
      Threshold: 10
      ActionsEnabled: false
      MetricName: ApproximateNumberOfMessagesVisible
  CanReceiveMessages:
    Type: AWS::IAM::Policy
    Properties:
      PolicyName: ReceiveSQSMessages
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Action: sqs:ReceiveMessage
            Resource: !GetAtt 'SyncQueue.Arn'
            Effect: Allow
      Roles:
        - !Ref 'ProdExecutionRole'
    Condition: IsProduction
  DeadLetterQueue:
    Type: AWS::SQS::Queue
    Properties:
      KmsMasterKeyId: !ImportValue 'KMSKeyStack-SQSKey'
      QueueName: !Sub '${Environment}-${QueueName}-DeadLetter'
      Tags:
        - Value: !Ref 'Environment'
          Key: Environment
  ProdExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
        - arn:aws:iam::aws:policy/AWSXrayWriteOnlyAccess
      AssumeRolePolicyDocument:
        Version: ''
        Statement:
          - Action: sts:AssumeRole
            Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
    Condition: IsProduction
  HandleMRMSyncEvent:
    Type: AWS::Lambda::Function
    Properties:
      TracingConfig:
        Mode: Active
      Code:
        S3Bucket: abbeycapital
        S3Key: MRM.SyncServerless/HandleMRMSyncEvent-CodeUri-637081391117261540-637081391307045466.zip
      VpcConfig:
        SubnetIds: !Ref 'Subnets'
        SecurityGroupIds:
          - !ImportValue
            Fn::Sub: ${Environment}-DatabaseSecurityGroup
      Tags:
        - Value: SAM
          Key: lambda:createdBy
        - Value: !Ref 'Environment'
          Key: Environment
      MemorySize: 256
      Environment:
        Variables:
          WarehouseSqlHostname: !Ref 'WarehouseDBHostName'
          SSMDatabasePasswordKey: !Ref 'DBPasswordSettingName'
          SqlHostname: !Ref 'DBHostname'
          SqlUser: mrm_lambda
      Handler: MRM.SyncServerless::MRM.SyncServerless.Functions::HandleSyncMessage
      Role: !If
        - IsProduction
        - !GetAtt 'ProdExecutionRole.Arn'
        - !Ref 'LambdaRole'
      Timeout: 30
      Runtime: dotnetcore3.1
  HandleMRMSyncEventHandleEvent:
    Type: AWS::Lambda::EventSourceMapping
    Properties:
      EventSourceArn: !GetAtt 'SyncQueue.Arn'
      FunctionName: !Ref 'HandleMRMSyncEvent'
