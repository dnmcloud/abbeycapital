AWSTemplateFormatVersion: '2010-09-09'
Description: Site to site VPN. Creation or use existing virtual private gateway.
Parameters:
  envPrefix:
    Description: Environment name prefix.
    Type: String
    Default: Shared-10.70
  MyVPCId:
    Description: The VPC reference.
    Type: String
  VPNAddress:
    Type: String
    Description: IP Address of the customer VPN device
    MinLength: '7'
    MaxLength: '15'
    AllowedPattern: (\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})
    ConstraintDescription: must be a valid IP address of the form x.x.x.x
  RouteTbls:
    Type: CommaDelimitedList
    Description: List existing Route Tables to propagate VPN routes to
  ExistVPNGateway:
    Type: String
    Description: Optional - ID of an existing cust. gateway, if empty a new one will
      be configured
  NewCustGatewaySite:
    Type: String
    Description: Optional - name for the site for the customer gateway
  OnPremiseCIDR:
    Type: String
    Description: IP Address range for the customer LAN that we will connect to over
      the VPN
    MinLength: '9'
    MaxLength: '18'
    AllowedPattern: (\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})
    ConstraintDescription: must be a valid IP CIDR range of the form x.x.x.x/x.
  OnPremiseCIDR2:
    Type: String
    Description: IP Address range for the customer LAN that we will connect to over
      the VPN
    MaxLength: '18'
  OnPremiseCIDR3:
    Type: String
    Description: IP Address range for the customer LAN that we will connect to over
      the VPN
    MaxLength: '18'
Conditions:
  CreateVPNG: !Equals [!Ref 'ExistVPNGateway', '']
  Create2ndRoute: !Not [!Equals [!Ref 'OnPremiseCIDR2', '']]
  Create3rdRoute: !Not [!Equals [!Ref 'OnPremiseCIDR3', '']]
Resources:
  VPNGateway:
    Type: AWS::EC2::VPNGateway
    Condition: CreateVPNG
    Properties:
      Type: ipsec.1
      Tags:
        - Key: Stack
          Value: !Ref 'AWS::StackName'
        - Key: Name
          Value: !Join ['-', [!Ref 'envPrefix', -VPG]]
  VPNGatewayAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Condition: CreateVPNG
    Properties:
      VpcId: !Ref 'MyVPCId'
      VpnGatewayId: !If [CreateVPNG, !Ref 'VPNGateway', !Ref 'ExistVPNGateway']
  CustGateway:
    Type: AWS::EC2::CustomerGateway
    Properties:
      Type: ipsec.1
      BgpAsn: '65000'
      IpAddress: !Ref 'VPNAddress'
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref 'envPrefix', !Ref 'NewCustGatewaySite']]
        - Key: Stack
          Value: !Ref 'AWS::StackName'
        - Key: VPN
          Value: !Join ['', ['Gateway to ', !Ref 'VPNAddress']]
  VPNConnection:
    Type: AWS::EC2::VPNConnection
    Properties:
      Type: ipsec.1
      StaticRoutesOnly: 'true'
      CustomerGatewayId: !Ref 'CustGateway'
      Tags:
        - Key: Name
          Value: !Join ['-', [!Ref 'envPrefix', Connection to, !Ref 'NewCustGatewaySite']]
        - Key: Stack
          Value: !Ref 'AWS::StackName'
        - Key: VPN
          Value: !Join ['', ['Connection to ', !Ref 'VPNAddress']]
      VpnGatewayId: !If [CreateVPNG, !Ref 'VPNGateway', !Ref 'ExistVPNGateway']
  VPNConnectionRoute:
    Type: AWS::EC2::VPNConnectionRoute
    Properties:
      VpnConnectionId: !Ref 'VPNConnection'
      DestinationCidrBlock: !Ref 'OnPremiseCIDR'
  VPNConnectionRoute2:
    Type: AWS::EC2::VPNConnectionRoute
    Condition: Create2ndRoute
    Properties:
      VpnConnectionId: !Ref 'VPNConnection'
      DestinationCidrBlock: !Ref 'OnPremiseCIDR2'
  VPNConnectionRoute3:
    Type: AWS::EC2::VPNConnectionRoute
    Condition: Create3rdRoute
    Properties:
      VpnConnectionId: !Ref 'VPNConnection'
      DestinationCidrBlock: !Ref 'OnPremiseCIDR3'
  myVPNGatewayRouteProp:
    Type: AWS::EC2::VPNGatewayRoutePropagation
    Condition: CreateVPNG
    Properties:
      RouteTableIds: !Ref 'RouteTbls'
      VpnGatewayId: !If [CreateVPNG, !Ref 'VPNGateway', !Ref 'ExistVPNGateway']
    DependsOn: VPNGatewayAttachment
Outputs: {}
