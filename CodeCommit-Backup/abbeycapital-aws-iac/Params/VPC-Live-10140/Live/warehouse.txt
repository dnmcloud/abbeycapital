Key
	Value
	Resolved value
CSVFLAGS	2_CSV_Flags/	-
Environment	test	-
PARQUETFLAGS	4_Parquet_Flags/	-
S3STAGINGBUCKET	abbeycapital-datawarehouse-staging	-
STAGINGCSV	1_Staging_CSV	-
STAGINGDB	staging_tables	-
STAGINGPARQUET	3_Staging_Parquet	-
TARGETCONCURRENCY	3	-
WAREHOUSEBUCKET	abbeycapital-datawarehousetest	-
WAREHOUSEDB	warehouse	-
WAREHOUSES3	5_Warehouse	-