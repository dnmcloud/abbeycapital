<Query Kind="Statements" />

long uploadBufferUsedInBytes = 130000;
long uploadBufferAllocatedInBytes = 999999;
var uploadBuffer = new
{
	UploadBufferUsedInBytes = uploadBufferUsedInBytes,
	UploadBufferAllocatedInBytes = uploadBufferAllocatedInBytes
};

var percentageUsed = ((double)uploadBuffer.UploadBufferUsedInBytes / (double)uploadBuffer.UploadBufferAllocatedInBytes ) * 100;
Console.WriteLine($"Upload buffer usage: {uploadBuffer.UploadBufferUsedInBytes} / {uploadBuffer.UploadBufferAllocatedInBytes} ({percentageUsed:0}%)");