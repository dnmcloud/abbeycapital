using Amazon.EC2;
using Amazon.Lambda.TestUtilities;
using Amazon.StorageGateway;
using Amazon.StorageGateway.Model;
using Moq;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using Amazon.EC2.Model;
using SGCreateSnapshotRequest = Amazon.StorageGateway.Model.CreateSnapshotRequest;
using SGCreateSnapshotResponse = Amazon.StorageGateway.Model.CreateSnapshotResponse;
using Amazon.CloudWatch;

namespace Snapshotter.Tests
{
    public static class AWSClients
    {
        private static readonly Mock<IAmazonEC2> _ec2Mock = new Mock<IAmazonEC2>();
        private static readonly Mock<IAmazonStorageGateway> _storageGatewayMock = new Mock<IAmazonStorageGateway>();
        private static readonly Mock<IAmazonCloudWatch> _cloudWatchMock = new Mock<IAmazonCloudWatch>();

        public static IAmazonEC2 EC2 => _ec2Mock.Object;
        public static IAmazonStorageGateway StorageGateway => _storageGatewayMock.Object;
        public static IAmazonCloudWatch CloudWatch => _cloudWatchMock.Object;
        public static Mock<IAmazonEC2> EC2Mock => _ec2Mock;
        public static Mock<IAmazonStorageGateway> StorageGatewayMock => _storageGatewayMock;
        public static Mock<IAmazonCloudWatch> CloudWatchMock => _cloudWatchMock;
        
        static AWSClients()
        {
            _ec2Mock
                .Setup(x => x.DescribeSnapshotsAsync(It.IsAny<DescribeSnapshotsRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => new DescribeSnapshotsResponse { Snapshots = new List<Snapshot> { } }); // Returns NO snapshots

            var listVolumesResponse = Responses.ListVolumesResponse;
            _storageGatewayMock
                .Setup(x => x.ListVolumesAsync(It.IsAny<ListVolumesRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => listVolumesResponse);

            // ListTagsForResourceAsync
            _storageGatewayMock
                .Setup(x => x.ListTagsForResourceAsync(It.IsAny<ListTagsForResourceRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => new ListTagsForResourceResponse { Tags = new List<Amazon.StorageGateway.Model.Tag>() });
            _storageGatewayMock
                .Setup(x => x.ListTagsForResourceAsync(It.Is<ListTagsForResourceRequest>(r => r.ResourceARN == listVolumesResponse.VolumeInfos[0].VolumeARN), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => Responses.DescribeTagsForResourceGood); // Tags the first volume in listVolumesResponse with Backup = Monthly

            _storageGatewayMock
                .Setup(x => x.DescribeUploadBufferAsync(It.IsAny<DescribeUploadBufferRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => Responses.DescribeUploadBufferRequestEmpty);

            _storageGatewayMock
                .Setup(x => x.CreateSnapshotAsync(It.IsAny<SGCreateSnapshotRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync((SGCreateSnapshotRequest request, CancellationToken token) => new SGCreateSnapshotResponse { SnapshotId = "snap-12345678", VolumeARN = request.VolumeARN });
        }
    }

    public class FunctionTest
    {
        [Fact]
        public async Task TestHappyPath()
        {
            var context = new TestLambdaContext();
            await new Function(AWSClients.StorageGateway, AWSClients.EC2, AWSClients.CloudWatch, "SG-ARN-HERE", "Backup", "Weekly").TakeSnapshot(new CloudWatchEvent(), context);

            AWSClients.EC2Mock.Verify(mock => mock.CreateTagsAsync(It.IsAny<CreateTagsRequest>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Fact]
        public async Task TestSnapshotChecker()
        {
            var context = new TestLambdaContext();
            await new Function(AWSClients.StorageGateway, AWSClients.EC2, AWSClients.CloudWatch, "SG-ARN-HERE", "Backup", "Weekly").CheckSnapshot(new CloudWatchEvent(), context);
        }

        [Fact]
        public async Task CanIdentifySnapshotCandidates()
        {
            var ec2mock = new Mock<IAmazonEC2>();
            ec2mock
                .Setup(x => x.DescribeSnapshotsAsync(It.IsAny<DescribeSnapshotsRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync((DescribeSnapshotsRequest request, CancellationToken cancellationToken) => Responses.DescribeSnapshots);

            var listVolumesResponse = Responses.ListVolumesResponse;
            var storageGatewayMock = new Mock<IAmazonStorageGateway>();
            storageGatewayMock
                .Setup(x => x.ListVolumesAsync(It.IsAny<ListVolumesRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => listVolumesResponse);

            // ListTagsForResourceAsync
            storageGatewayMock
                .Setup(x => x.ListTagsForResourceAsync(It.IsAny<ListTagsForResourceRequest>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => new ListTagsForResourceResponse { Tags = new List<Amazon.StorageGateway.Model.Tag>() });
            storageGatewayMock
                .Setup(x => x.ListTagsForResourceAsync(It.Is<ListTagsForResourceRequest>(r => r.ResourceARN == listVolumesResponse.VolumeInfos[0].VolumeARN), It.IsAny<CancellationToken>()))
                .ReturnsAsync(() => Responses.DescribeTagsForResourceGood);
              
            var ec2 = ec2mock.Object;
            var snapshots = await ec2.DescribeSnapshotsAsync(new DescribeSnapshotsRequest { /* Whatever here... */ });

            var storageGateway = storageGatewayMock.Object;
            var snapshotter = new SnapshotSystem(storageGateway, ec2);
            var snapshotCandidateVolumes = await snapshotter.GetVolumesWithTagKey("DUMMY-SG-ARN", "Backup");

            Assert.True(snapshotCandidateVolumes.Any());
        }
    }

    internal static class Responses
    {
        public static DescribeSnapshotsResponse DescribeSnapshots => JsonConvert.DeserializeObject<DescribeSnapshotsResponse>(File.ReadAllText("Responses/DescribeSnapshotResponse.json"));
        public static ListVolumesResponse ListVolumesResponse => JsonConvert.DeserializeObject<ListVolumesResponse>(File.ReadAllText("Responses/StorageGatewayListVolumesResponse.json"));
        public static ListTagsForResourceResponse DescribeTagsForResourceGood => JsonConvert.DeserializeObject<ListTagsForResourceResponse>(File.ReadAllText("Responses/DescribeTagsForResourceResponseGood.json"));
        public static DescribeUploadBufferResponse DescribeUploadBufferRequestEmpty => JsonConvert.DeserializeObject<DescribeUploadBufferResponse>(File.ReadAllText("Responses/DescribeUploadBufferResponseEmpty.json"));
    }
}
