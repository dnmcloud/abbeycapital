# Storage Gateway Snapshots

## Background

Our on-premise servers are backed up via Veeam to a Cached Volume Storage Gateway instance. Storage Gateway presents this volume as a 10TB iSCSI device which is mounted onto Abbey67. As Veeam writes backups to this device, they are uploaded to a volume stored in AWS. To handle the bandwidth bottleneck between our local datacenter and AWS, Storage Gateway uses an upload buffer so that Veeam's disk writes are not blocked.

`(Application Throughput (MB/s) - Network Throughput to AWS (MB/s) * Compression Factor) * Duraction of writes (s) = Upload Buffer (MB)` [1]

Note: Compression factor here can be 2:1 for text, but in the case of binary files outputted by Veeam, we should assume that it is 1.

On a monthly basis, Veeam makes a full backup on a Saturday and incrementals for each subsequent day which it retains for a month. On the last Saturday of a month, if we snapshot the volume, then we can retrieve data for any day in that month.

To retrieve a backup from a given month, a volume can be created from the snapshot and mounted to Veeam. From there, to usual backup recovery process can continue.

## Requirements

* Monthly snapshots of Storage Gateway volume on a schedule
* Needs to ensure that Storage Gateway upload buffer is not full before proceeding
* Tag and name snapshots for identification
* KMS encrypted
* List snapshots taken
* Provide process to attach snapshots to Storage Gateway

## Components

### Snapshotter

This process will run on a given day at an interval and attempt to make a snapshot of a volume

![SnapshotterFlowchart](SnapshotterFlowchart.png)

#### What gets snapshotted?

Storage Gateway volumes we want snapshotted will be tagged

### When

[Cron expressions input into Cloudformation stack](https://docs.aws.amazon.com/AmazonCloudWatch/latest/events/ScheduledEvents.html#CronExpressions)

### Checker

This process will check to see if a snapshot has been taken and notify the IT department as appropriate

#### How do we know if a snapshot was taken?

The same tag referenced above, along with a month and year tag will be added to the snapshot taken

### BufferFullAlarm

If upload buffer on Storage Gateway exceeds a preset value, an alert will be sent to ITAlerts

### DiskFullAlarm

Monitor disk usage of backup volumes

## How to deploy

[Developer setup](SETUP.md)

## References

[1]: https://docs.aws.amazon.com/storagegateway/latest/userguide/ManagingLocalStorage-common.html#CachedLocalDiskUploadBufferSizing-common

## CLI Commands (Sample)

`aws storagegateway describe-gateway-information --gateway-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59 --profile admin`
Returns, amongst other things, the availble IPV4 interfaces, one of which must be specified below.

`aws storagegateway create-cached-iscsi-volume --gateway-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59 --volume-size-in-bytes 1099511627776 --network-interface-id 192.168.1.94 --profile admin --target-name testvolume --client-token 31337`
Returns target arn and volume arn

`aws storagegateway create-snapshot --volume-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59/volume/vol-0EAA5701C4F6887DF --snapshot-description "Test snapshot" --profile admin`
Returns volume arn and snapshot id

`aws ec2 create-tags --resources snap-0f9de21b4de250587 --tags Key=Purpose,Value=Backup Key=Month,Value=Feb Key=Year,Value=2019 --profile admin`

`aws storagegateway add-tags-to-resource --resource-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59/volume/vol-0EAA5701C4F6887DF --tags Name=Backup,Value="" --profile admin` 

Find volumes that need to be snapshotted via tags

`aws storagegateway list-volumes --gateway-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59 --query VolumeInfos[*].VolumeARN --profile admin`
`aws storagegateway list-tags-for-resource --resource-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59/volume/vol-0EAA5701C4F6887DF --profile admin`

Enumerate snapshots

`aws ec2 describe-snapshots --filters Name=tag-key,Values="Month,Year" Name=tag:Purpose,Values=Backup --profile admin`


This used to create encrypted volume
`aws storagegateway create-cached-iscsi-volume --gateway-arn arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59 --volume-size-in-bytes 1099511627776 --network-interface-id 192.168.1.94 --profile admin --target-name testvolumeencrypted --client-token 131337 --kms-encrypted --kms-key "arn:aws:kms:eu-west-1:985715988019:key/565c257f-3b13-49f6-a2b1-bc9ba91580b2" --snapshot-id snap-099642c01ec4628d8 --debug`

Find snapshots

aws ec2 describe-snapshots
    --filters Name=tag:Month,Values="February" Name=tag:Year,Values=2019 Name=tag-key,Values=Backup
    --query Snapshots[*].{VolumeId:VolumeId,SnapshotId:SnapshotId,Tags:Tags}