# Debugging and deploying Serverless Applications written in C\# 

## Software Requirements

* [Install Python](https://www.python.org/downloads/) and make sure it is on your path
* [Install the AWS Lambda Tools for .Net](https://github.com/aws/aws-extensions-for-dotnet-cli#aws-lambda-amazonlambdatools)
* [Install AWS project templates](https://aws.amazon.com/blogs/developer/creating-net-core-aws-lambda-projects-without-visual-studio/)
* [Install Docker for Windows](https://www.docker.com/products/docker-desktop)

## Setup

* Install Python modules
* Configure AWS CLI for MFA
* Install .Net debugger into AWS Lambda docker image

### Install Python modules

Create a self-contained Python environment for AWS stuff, can be reused elsewhere

```bash
python -m pip install virtualenv --user
python -m virtualenv c:\Tools\awsenv
c:\Tools\awsenv\Scripts\activate
cd PATH\TO\GIT\REPOSITORY
pip install -r requirements.txt
```

### Configure AWS CLI for MFA

Configure a profile with this command: `aws configure --profile default-long-term`

Open `%USERPROFILE%\.aws\credentials` and find the default-long-term profile. Add a reference to your MFA. The format for virtual MFAs is arn:aws:iam::985715988019:mfa/YOUR_USERNAME_HERE

Your profile should look like this

```
[default-long-term]
aws_access_key_id = MY_ACCESS_KEY_ID
aws_secret_access_key = MY_SECRET_ACCESS_KEY
region = eu-west-1
aws_mfa_device = arn:aws:iam::985715988019:mfa/nmackey
toolkit_artifact_guid = 263ff4ee-04a1-4a53-8187-092b0a4db573
```

Once you've done this, use aws-mfa to access AWS resources from the CLI or API.

```
aws s3 ls
aws-mfa
aws s3 ls
```

### Install .Net debugger

```bash
set DEBUGGER_PATH=c:\Tools\debugger
mkdir %DEBUGGER_PATH%
docker run --rm --mount type=bind,src=%DEBUGGER_PATH%,dst=/vsdbg --entrypoint bash lambci/lambda:dotnetcore2.0 -c "curl -sSL https://aka.ms/getvsdbgsh | bash /dev/stdin -v latest -l /vsdbg"
```

## Debugging

* Package application
* Invoke Lambda locally with AWS SAM
* Attach debugger

### Package application 

Build the application in debug mode. Make sure that you output the applicaiton to the path referenced in the code uri property of `template.yaml`

```bash
dotnet lambda package -c Debug --framework netcoreapp2.1 --project-location Source/Snapshotter -o artifacts/Snapshotter.zip
```

### Invoke Lambda locally

You can invoke Lambdas locally using SAM and Docker. The invoke command will accept a JSON payload, which the handler will deserialise.

Generate event skeletons to pass to Lambdas with `sam local generate-event`

Once you have configured your event run:

```bash
sam local invoke --event cloudwatchevent.json --env-vars environment.json -d 5555 --debugger-path %DEBUGGER_PATH% --profile abbey TakeSnapshotFunction
```

## Deploy

* Package application
* Prepare template
* Deploy to Cloudformation

```bash
dotnet lambda package -c Release --framework netcoreapp2.1 --project-location Source/Snapshotter -o artifacts/Snapshotter.zip
aws cloudformation package --template-file template.yaml --output-template-file packaged.yaml --s3-bucket abbeycapital-codedeploy
aws cloudformation deploy --template-file packaged.yaml --stack-name Infrastructure-StorageGatewaySnapshotter
```

## Testing

All Amazon client classes are coded against an interface. So `AmazonStorageGatewayClient` implements `IAmazonStorageGateway`. If you work with interfaces instead of concrete classes in your code, you can mock up instances of IAmazonStorageGateway etc for testing purposes. Moq is very helpful here.

Here's an example of mocking a call to `IAmazonStorageGateway.ListVolumesAsync`:

```csharp
var dummyResponse = new ListVolumesResponse
{
    VolumeInfos = new List<VolumeInfo>
    {
        new VolumeInfo { VolumeARN = string.Join("", Enumerable.Repeat('a', 50)) }
    }
};

var storageGateway = new Mock<IAmazonStorageGateway>();
storageGateway
    .Setup(x => x.ListVolumesAsync(It.IsAny<ListVolumesRequest>(), It.IsAny<CancellationToken>()))
    .ReturnsAsync((ListVolumesRequest request, CancellationToken cancellationToken) => dummyResponse);
            
var response = await storageGateway.Object.ListVolumesAsync(new ListVolumesRequest { GatewayARN = string.Join("", Enumerable.Repeat('a', 50)) });
```

It's also possible to deserialise the JSON output of AWS commands from the CLI into AWS SDK response objects and return those in mocks.

```csharp
internal static class Responses
{
    public static DescribeUploadBufferResponse DescribeUploadBufferRequestEmpty => JsonConvert.DeserializeObject<DescribeUploadBufferResponse>(File.ReadAllText("Responses/DescribeUploadBufferResponseEmpty.json"));
}
```

Then later you can do this:

```csharp
_storageGatewayMock
    .Setup(x => x.DescribeUploadBufferAsync(It.IsAny<DescribeUploadBufferRequest>(), It.IsAny<CancellationToken>()))
    .ReturnsAsync(() => Responses.DescribeUploadBufferRequestEmpty);
```

Where `DescribeUploadBufferResponseEmpty.json` looks like this, for example.

```json
{
    "GatewayARN": "arn:aws:storagegateway:eu-west-1:985715988019:gateway/sgw-300EEA59",
  "DiskIds": [
    "d7333fcb-616f-4b3c-a63e-62f809443819",
    "7f515f9c-97c8-4d62-be55-2ab4bfaf07cf"
  ],
    "UploadBufferUsedInBytes": 0,
    "UploadBufferAllocatedInBytes": 536870912000
}
```

## Troubleshooting

* Viewing logs
* Viewing traces in XRay

## Serverless Application Model

[AWS Serverless Application Model one pager](https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md)