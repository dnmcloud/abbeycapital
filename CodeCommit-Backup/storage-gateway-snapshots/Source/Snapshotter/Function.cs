using Amazon.CloudWatch;
using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.Lambda.Core;
using Amazon.StorageGateway;
using Amazon.StorageGateway.Model;
using Amazon.XRay.Recorder.Handlers.System.Net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Snapshotter
{
    public class Detail
    {

    }

    public class CloudWatchEvent
    {
        public string Account { get; set; }
        public string Region { get; set; }
        public Detail Detail { get; set; }
        public string Source { get; set; }
        public DateTime Time { get; set; }
        public string Id { get; set; }
        public string[] Resources { get; set; }
    }

    enum BackupInterval
    {
        Weekly,
        Monthly
    }

    public class Function
    {
        private readonly IAmazonStorageGateway _storageGatewayClient;
        private readonly IAmazonEC2 _ec2Client;
        private readonly IAmazonCloudWatch _cloudwatchClient;
        private readonly string _gatewayArn;
        private readonly string _tagKey;
        private readonly BackupInterval _interval;

        public Function() : this(
            new AmazonStorageGatewayClient(),
            new AmazonEC2Client(),
            new AmazonCloudWatchClient(),
            Environment.GetEnvironmentVariable("GATEWAY_ARN"),
            Environment.GetEnvironmentVariable("TAG_KEY"),
            Environment.GetEnvironmentVariable("INTERVAL"))
        {
        }

        public Function(IAmazonStorageGateway storageGateway, IAmazonEC2 ec2, IAmazonCloudWatch cloudwatch, string gatewayArn, string tagKey, string interval)
        {
            _storageGatewayClient = storageGateway ?? throw new ArgumentNullException(nameof(storageGateway));
            _ec2Client = ec2 ?? throw new ArgumentNullException(nameof(ec2));
            _cloudwatchClient = cloudwatch ?? throw new ArgumentNullException(nameof(cloudwatch));
            _gatewayArn = gatewayArn;
            _tagKey = tagKey;
            
            if (Enum.TryParse<BackupInterval>(
                value:interval,
                ignoreCase:true,
                result:out _interval
            ) == false)
            {
                _interval = BackupInterval.Monthly;
                Console.WriteLine($"Failed to parse {typeof(BackupInterval)} from {interval}. assuming {_interval}");
            }
        }

        static Function()
        {
            Amazon.XRay.Recorder.Handlers.AwsSdk.AWSSDKHandler.RegisterXRayForAllServices();
        }

        public async Task TakeSnapshot(CloudWatchEvent input, ILambdaContext context)
        {
            var snapshotSystem = new SnapshotSystem(_storageGatewayClient, _ec2Client);
            var volumesToSnapshot = await GetVolumesToSnapshot(snapshotSystem);

            if (volumesToSnapshot.Any() == false)
            {
                Console.WriteLine($"All snapshots complete for the following interval: {_interval}.");
                await PutSnapshotDoneMetric();
                return;
            }

            var uploadBuffer = await _storageGatewayClient.DescribeUploadBufferAsync(new DescribeUploadBufferRequest
            {
                GatewayARN = _gatewayArn,
            });

            Console.WriteLine($"Upload buffer usage: {uploadBuffer.UploadBufferUsedInBytes} / {uploadBuffer.UploadBufferAllocatedInBytes} ({uploadBuffer.PercentUsed():0}%)");

            if (uploadBuffer.UploadBufferUsedInBytes > 0)
            {
                Console.WriteLine("Buffer in use, not taking a snapshot");
                await PutUploadBufferMetric(uploadBuffer);
                return;
            }

            foreach (var volume in volumesToSnapshot)
            {
                Console.WriteLine($"About to snapshot {volume.VolumeInfo.VolumeARN}");
                try
                {
                    await snapshotSystem.TakeSnapshot(volume.VolumeInfo.VolumeARN, volume.Tags.Single(t => t.Key == _tagKey));
                    await PutSnapshotTakenMetric(volume.VolumeInfo.VolumeId);
                }
                catch
                {
                    continue;
                }
            }
        }

        public async Task CheckSnapshot(CloudWatchEvent input, ILambdaContext context)
        {
            var snapshotSystem = new SnapshotSystem(_storageGatewayClient, _ec2Client);
            var volumesToSnapshot = await GetVolumesToSnapshot(snapshotSystem);

            if (volumesToSnapshot.Any() == false)
            {
                return;
            }

            var httpClient = new HttpClient(new HttpClientXRayTracingHandler(new HttpClientHandler()));
            string slackEndpoint = Environment.GetEnvironmentVariable("SLACK_WEBHOOK");
            var text = "Storage gateway snapshots still pending";
            var payload = JsonConvert.SerializeObject(new
            {
                icon_emoji = ":scorpion:",
                username = "AWS Alerts",
                attachments = new []
                {
                    new
                    {
                        pretext = text,
                        color = "#D00000",
                        fallback = text,
                        fields = volumesToSnapshot.Select(v => new { title = "Volume Id", value = v.VolumeInfo.VolumeId, Short = false } ).ToArray()
                    }
                }
            });
            await httpClient.PostAsync(slackEndpoint, new StringContent(payload));
        }

        private async Task<IEnumerable<VolumeInfoWithTags>> GetVolumesToSnapshot(SnapshotSystem snapshotSystem)
        {
            var volumes = await snapshotSystem.GetVolumesWithTagKey(_gatewayArn, _tagKey);

            var now = DateTime.Now;
            var filters = new List<Filter>
            {
                new Filter { Name = "tag:Month", Values = new List<string> { $"{now:MMMM}" } },
                new Filter { Name = "tag:Year", Values = new List<string> { $"{now:yyyy}" } },
                new Filter { Name = "tag-key", Values = new List<string> { _tagKey } },
            };
            if (_interval == BackupInterval.Weekly)
            {
                filters.Add(new Filter { Name = "tag:WeekOfYear", Values = new List<string> { $"{now.GetWeekOfYear()}" } });
            }

            var snapshots = await _ec2Client.DescribeSnapshotsAsync(new DescribeSnapshotsRequest
            {
                Filters = filters
            });

            foreach (var snapshot in snapshots.Snapshots)
            {
                var tags = string.Join(", ", snapshot.Tags.Select(t => $"{t.Key}:{t.Value}"));
                Console.WriteLine($"VolumeId: {snapshot.VolumeId} SnapshotId: {snapshot.SnapshotId} Tags: {tags}");
            }

            var snapshotVolumeIds = snapshots.Snapshots.Select(s => s.VolumeId);
            var volumesToSnapshot = volumes.Where(v => snapshotVolumeIds.Contains(v.VolumeInfo.VolumeId) == false);
            return volumesToSnapshot;
        }

        private async Task<Amazon.CloudWatch.Model.PutMetricDataResponse> PutUploadBufferMetric(DescribeUploadBufferResponse uploadBuffer)
        {
            return await _cloudwatchClient.PutMetricDataAsync(new Amazon.CloudWatch.Model.PutMetricDataRequest
            {
                Namespace = "abbey/snapshotter",

                MetricData = new List<Amazon.CloudWatch.Model.MetricDatum>
                    {
                        new Amazon.CloudWatch.Model.MetricDatum
                        {
                            MetricName = "BufferInUse",
                            Unit = StandardUnit.Bytes,
                            Value = uploadBuffer.UploadBufferUsedInBytes,
                            Dimensions = new List<Amazon.CloudWatch.Model.Dimension>
                            {
                                new Amazon.CloudWatch.Model.Dimension
                                {
                                    Name = "GatewayArn",
                                    Value = _gatewayArn,
                                }
                            }
                        },
                    }
            });
        }

        private async Task<Amazon.CloudWatch.Model.PutMetricDataResponse> PutSnapshotDoneMetric()
        {
            return await _cloudwatchClient.PutMetricDataAsync(new Amazon.CloudWatch.Model.PutMetricDataRequest
            {
                Namespace = "abbey/snapshotter",
                MetricData = new List<Amazon.CloudWatch.Model.MetricDatum>
                {
                    new Amazon.CloudWatch.Model.MetricDatum
                    {
                        Value = 1,
                        MetricName = "SnapshotAlreadyDone",
                        Dimensions = new List<Amazon.CloudWatch.Model.Dimension>
                        {
                            new Amazon.CloudWatch.Model.Dimension
                            {
                                Name = "GatewayArn",
                                Value = _gatewayArn,
                            }
                        }
                    },
                }
            });
        }

        private async Task<Amazon.CloudWatch.Model.PutMetricDataResponse> PutSnapshotTakenMetric(string volumeId)
        {
            return await _cloudwatchClient.PutMetricDataAsync(new Amazon.CloudWatch.Model.PutMetricDataRequest
            {
                Namespace = "abbey/snapshotter",
                MetricData = new List<Amazon.CloudWatch.Model.MetricDatum>
                {
                    new Amazon.CloudWatch.Model.MetricDatum
                    {
                        Value = 1,
                        MetricName = "SnapshotTaken",
                        Dimensions = new List<Amazon.CloudWatch.Model.Dimension>
                        {
                            new Amazon.CloudWatch.Model.Dimension
                            {
                                Name = "GatewayArn",
                                Value = _gatewayArn,
                            },
                            new Amazon.CloudWatch.Model.Dimension
                            {
                                Name = "VolumeId",
                                Value = volumeId,
                            }
                        }
                    },
                }
            });
        }
    }
}

// dotnet lambda package -c Debug --framework netcoreapp2.1 --project-location Source/Snapshotter -o artifacts/Snapshotter.zip & sam package --template-file template.yaml --output-template-file packaged.yaml --s3-bucket abbeycapital-codedeploy --profile abbey & aws cloudformation deploy --template-file packaged.yaml --stack-name Infrastructure-StorageGatewaySnapshotter --profile abbey

// https://docs.aws.amazon.com/storagegateway/latest/userguide/sg-api-permissions-ref.html
// https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-creating-snapshot.html