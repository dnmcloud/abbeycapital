﻿using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.StorageGateway;
using Amazon.StorageGateway.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SGCreateSnapshotRequest = Amazon.StorageGateway.Model.CreateSnapshotRequest;
using EC2Tag = Amazon.EC2.Model.Tag;
using System.Linq;
using SGTag = Amazon.StorageGateway.Model.Tag;
using System.Globalization;

namespace Snapshotter
{
    public class VolumeInfoWithTags
    {
        public VolumeInfo VolumeInfo { get; set; }
        public List<SGTag> Tags { get; set; }
    }

    public static class AWSModelExtensions
    {
        public static double PercentUsed(this DescribeUploadBufferResponse uploadBufferInfo)
        {
            return ((double)uploadBufferInfo.UploadBufferUsedInBytes / (double)uploadBufferInfo.UploadBufferAllocatedInBytes ) * 100;
        }
    }

    
    public static class DateStuff
    {
        private static CultureInfo CultureInfo = new CultureInfo("en-IE");
        
        public static int GetWeekOfYear(this DateTime dateTime)
        {
            var weekRule = CultureInfo.DateTimeFormat.CalendarWeekRule;
            var firstDayOfWeek = CultureInfo.DateTimeFormat.FirstDayOfWeek;
            var calendar = CultureInfo.Calendar;
            return calendar.GetWeekOfYear(DateTime.Now, weekRule, firstDayOfWeek);
        }
    }

    public class SnapshotSystem
    {
        private readonly IAmazonStorageGateway _storageGatewayClient;
        private readonly IAmazonEC2 _ec2Client;

        public SnapshotSystem(IAmazonStorageGateway storageGatewayClient, IAmazonEC2 ec2Client)
        {
            _storageGatewayClient = storageGatewayClient ?? throw new ArgumentNullException(nameof(storageGatewayClient));
            _ec2Client = ec2Client ?? throw new ArgumentNullException(nameof(ec2Client)); 
        }

        public async Task<IEnumerable<VolumeInfoWithTags>> GetVolumesWithTagKey(string gatewayArn, string tagKey)
        {
            try
            {
                var volumes = await _storageGatewayClient.ListVolumesAsync(new ListVolumesRequest
                {
                    GatewayARN = gatewayArn, 
                });

                var snapshotCandidateVolumes = new List<VolumeInfoWithTags>();
                foreach (var volume in volumes.VolumeInfos)
                {
                    var tags = await _storageGatewayClient.ListTagsForResourceAsync(new ListTagsForResourceRequest { ResourceARN = volume.VolumeARN });
                    if (tags.Tags.Any(t => t.Key == tagKey) == false)
                    {
                        continue;
                    }

                    snapshotCandidateVolumes.Add(new VolumeInfoWithTags { VolumeInfo = volume, Tags = tags.Tags.Select(t => new SGTag { Key = t.Key, Value = t.Value }).ToList()});
                }

                return await Task.FromResult(snapshotCandidateVolumes);
            }
            catch (InvalidGatewayRequestException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public async Task<string> TakeSnapshot(string volumeArn, SGTag tag)
        {
            try
            {
                var now = DateTime.Now;
                var snapshotCreateResult = await _storageGatewayClient.CreateSnapshotAsync(new SGCreateSnapshotRequest
                { 
                    VolumeARN = volumeArn,
                    SnapshotDescription = $"Snapshotter - {tag.Key}:{tag.Value}",
                });
                var createTagResult = await _ec2Client.CreateTagsAsync(new CreateTagsRequest
                {
                    Resources = new List<string> { snapshotCreateResult.SnapshotId },
                    Tags = new List<EC2Tag>
                    {
                        new EC2Tag { Key = "Month", Value = $"{now:MMMM}" },
                        new EC2Tag { Key = "Year", Value = $"{now:yyyy}" },
                        new EC2Tag { Key = "WeekOfYear", Value = $"{now.GetWeekOfYear()}" },
                        new EC2Tag { Key = tag.Key, Value = tag.Value },
                    },
                });
                return await Task.FromResult(snapshotCreateResult.SnapshotId);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to snapshot volume {volumeArn} {ex.Message}");
                throw;
            }
        }
    }
}
