[CmdletBinding()]
param(

    [Parameter(Mandatory=$true)]
    [string]$DCBIOSName,

    [Parameter(Mandatory=$true)]
    [string]$DCUsername,

    [Parameter(Mandatory=$true)]
    [string]$DCPassword,

    [Parameter(Mandatory=$true)]
    [string]$MyWindowsNameAZ1,

    [Parameter(Mandatory=$true)]
    [string]$MyWindowsNameAZ2,

    [Parameter(Mandatory=$false)]
    [string]$WSFCNode3NetBIOSName=$null

)
$success = $false
For ($i=0; $i -le 4; $i++) {
    if ($success -eq $true) {
        Break
    }

    try {
        Start-Transcript -Path C:\cfn\log\Enable-SqlAlwaysOn.ps1.txt -Append
        $ErrorActionPreference = "Stop"

        $DomainAdminFullUser = $DCBIOSName + '\' + $DCUsername
        $DomainAdminSecurePassword = ConvertTo-SecureString $DCPassword -AsPlainText -Force
        $DomainAdminCreds = New-Object System.Management.Automation.PSCredential($DomainAdminFullUser, $DomainAdminSecurePassword)

        $EnableAlwaysOnPs={
            $ErrorActionPreference = "Stop"
            Set-ExecutionPolicy -Scope Process -ExecutionPolicy RemoteSigned
            Enable-SqlAlwaysOn -ServerInstance $Using:serverInstance -Force
        }

        $serverInstance = $MyWindowsNameAZ1
        Invoke-Command -Scriptblock $EnableAlwaysOnPs -ComputerName $MyWindowsNameAZ1 -Credential $DomainAdminCreds
        $serverInstance = $MyWindowsNameAZ2
        Invoke-Command -Scriptblock $EnableAlwaysOnPs -ComputerName $MyWindowsNameAZ2 -Credential $DomainAdminCreds
        if ($WSFCNode3NetBIOSName) {
            $serverInstance = $WSFCNode3NetBIOSName
            Invoke-Command -Scriptblock $EnableAlwaysOnPs -ComputerName $WSFCNode3NetBIOSName -Credential $DomainAdminCreds
        }

        $success = $true

    }
    catch {
        if ($i -eq 3) {
          Write-Verbose "$($_.exception.message)@ $(Get-Date)"
        }
        else {
            $success = $false
        }
    }

    Start-Sleep -s 300
}
