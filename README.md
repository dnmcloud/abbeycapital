Repository moved to AbbeyCapital's AWS CodeCommit.

All changes must be pushed to 'abbeycapital-aws-iac' repo:

https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/abbeycapital-aws-iac

xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
Justine requested that we store a backup copy of their CodeCommit repository
https://deloitteiece.atlassian.net/browse/DNM-540355
abbeycapital-aws-iac (currently used)
storage-gateway-snapshots (seems like not used for number of years but copying anyway)

abbeycapital-aws-iac has two branches in CodeCommit so abbeycapital-aws-iac is master
abbeycaptial-aws-iac-Branch-allow_bitbucket_to_access_aurora is allow_bitbucket_to_access_aurora branch 
storage-gateway-snapshots has only master branch.

This is temporary, as AbbeyCapital plan to create another account for backups, in case main account gets compromised, and then the CodeCommit will be setup to cross account replicate.

